package com.facebookfriendfinder;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;

public class FindFriendActivity extends Activity {

	WebView addFriendPrompt;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_friend);
		addFriendPrompt =  (WebView) findViewById(R.id.addFriendPrompt);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.find_friend, menu);
		return true;
	}
	
	public void scanItClicked(View veiw) {
		try {
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
	        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
	        startActivityForResult(intent, 0);
		} catch (Exception e) {
			Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
			Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
			startActivity(marketIntent);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
	    if (requestCode == 0) {
	        if (resultCode == RESULT_OK) {
	            String contents = intent.getStringExtra("SCAN_RESULT");
	            String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
	            Log.i("info", contents);
	            // Handle successful scan
	            //until dialog can be figure out use hack solution of going to persons profile
//	            addFriendPrompt.loadUrl("http://www.facebook.com/dialog/friends/?"+
//										  "id=" + contents +
//										  "&app_id="+R.string.app_id+
//										  "&redirect_uri=https://facebook.com/");
	            addFriendPrompt.loadUrl(contents);
	        } else if (resultCode == RESULT_CANCELED) {
	            // Handle cancel
	        }
	    }
	}
}
