package com.facebookfriendfinder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;

import android.R.bool;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

public class MainActivity extends Activity {

	private TextView loginMessage;
	Bundle bundle = new Bundle();
	boolean isConnected;
	SharedPreferences profile;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		loginMessage = (TextView) findViewById(R.id.login_message);
		profile = getSharedPreferences("myProfile", 0);
		
		  try {
		        PackageInfo info = getPackageManager().getPackageInfo(
		                "com.facebookfriendfinder", 
		                PackageManager.GET_SIGNATURES);
		        for (Signature signature : info.signatures) {
		            MessageDigest md = MessageDigest.getInstance("SHA");
		            md.update(signature.toByteArray());
		            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		            }
		    } catch (NameNotFoundException e) {
		    	 Log.d("KeyHash:", "exception name not found");
		    } catch (NoSuchAlgorithmException e) {
		    	 Log.d("KeyHash:", "exception no such algorithm");
		    }
		

		//need to track connected so handle those conditions
		Session.openActiveSession(this, true, new Session.StatusCallback() {
			
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				onSessionStateChange(session, state, exception);
				if (session.isOpened()) {
					isConnected = true;
					Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
						@Override
			            public void onCompleted(GraphUser user, Response response) {
							try {
								Log.i("user_id", response.getGraphObject().getInnerJSONObject().getString("id"));
								bundle.putString("user_id", response.getGraphObject().getInnerJSONObject().getString("id"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								Log.i("user_id", "error");
								e.printStackTrace();
							}
						}
					});
				} else {
					Log.i("user_id", "not connecting");
					isConnected = false;
				}
			}
		});
	}
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
			loginMessage.setText(R.string.logged_in);
	    } else if (state.isClosed()) {
			loginMessage.setText(R.string.logged_out);
	    }
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}
	
	public void getQRClicked(View view) {
		Intent intent = new Intent(this, GetQRActivity.class);
		bundle.putBoolean("isConnected", isConnected);
		intent.putExtras(bundle);
        startActivity(intent);
	}
	
	public void findFriendClicked(View view) {
		Log.i(STORAGE_SERVICE, "Cicked Friend Finder");
		Intent intent = new Intent(this, FindFriendActivity.class);
        startActivity(intent);
	}
}
