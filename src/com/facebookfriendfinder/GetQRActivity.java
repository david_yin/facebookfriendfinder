package com.facebookfriendfinder;

import org.json.JSONException;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

import com.google.zxing.integration.android.*;

public class GetQRActivity extends Activity {
	TextView qrCodeMessage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_qr);
		qrCodeMessage = (TextView) findViewById(R.id.qrMessage);
		getQRCode();
	}

	private void getQRCode() {
		//grabs the qr code from from online if exists disconnected get from shared preferences
		SharedPreferences profile = getSharedPreferences("myProfile", 0);
		final SharedPreferences.Editor editor = profile.edit();
		String value;
		if (getIntent().getExtras().getBoolean("isConnected")){
			Log.i("user_id", "in generate");
			//create a new url request
			//for now we're using inefficient calls to facebook
			Bundle getData = getIntent().getExtras();
			value = getData.getString("user_id");
			editor.putString("user_id", "http://www.facebook.com/profile.php?"+
					"id=" + value);
			editor.commit();
			Log.i("user_id",value);
			generateQR(profile.getString("user_id", "Could Not Find URL"));
		} else if (profile.contains("user_id")) {
			Log.i("user_id", "in contains");
			value = profile.getString("user_id", "Could Not Find URL");
			Log.i("user_id",value);
			generateQR(value);
		} else {
			qrCodeMessage.setText(R.string.not_connected);
		}
	}
	
	private void generateQR(String value) {
		//generate new QR code from URL request
		IntentIntegrator integrator = new IntentIntegrator(this);
		integrator.shareText(value);		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.get_qr, menu);
		return true;
	}

}
